#pragma once
/*
/////////////////////////////INFORMACION A COMPARTIR//////////////////////////////
-Posición de la raqueta
-Posición de la pelota
-Acción del bot: 1 = arriba, 0 = nada, -1 = abajo
*/
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida{
    public:
    Esfera esfera;
    Raqueta raqueta1;
    int accion; 
};