#include "Plano.h"
#include "Vector2D.h"

typedef enum {derecha, izquierda} direccion;

class Disparo : public Plano{
    public:
        //Atributos
        Vector2D velocidad;
        direccion dir;

        //Metodos
        Disparo(direccion d, Vector2D pos);
        virtual ~Disparo(){}
        void Mueve(float t );
        void Dibuja();

};