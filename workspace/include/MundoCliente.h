#pragma once

#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Disparo.h"
#include "DatosMemCompartida.h"

class MundoCliente
{
public:
    //Atributos
    Esfera esfera;
    std::vector<Esfera*> esferas; //se mueven
    Plano fondo_izq;
    Plano fondo_dcho;
    std::vector<Plano> paredes;
    std::vector<Disparo*> disparos; //se mueven
    Raqueta jugador1;
    Raqueta jugador2;
    DatosMemCompartida datosCompartidos; //atributo de datos compartidos
    DatosMemCompartida *pdatosCompartidos; //puntero a datos compartidos
    int puntos1;
    int puntos2; 
    int fifo_servidor_cliente; //descriptor de fichero del fifo que envia informacion del servidor al cliente
    int fifo_cliente_servidor; //fd del fifo que envia la tecla del cliente al servidor

    //Metodos
    MundoCliente();
    virtual ~MundoCliente();
    void Init();
    void InitGL();
    void OnKeyboardDown(unsigned char key, int x, int y);
    void OnTimer(int value);
    void OnDraw();
};
