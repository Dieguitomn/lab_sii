#pragma once

#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Disparo.h"


class MundoServidor
{
public:
    //Atributos
    Esfera esfera;
    std::vector<Esfera*> esferas; //se mueven
    Plano fondo_izq;
    Plano fondo_dcho;
    std::vector<Plano> paredes;
    std::vector<Disparo*> disparos; //se mueven
    Raqueta jugador1;
    Raqueta jugador2;
    int puntos1;
    int puntos2; 
    int logger; //descriptor de fichero del fifo que se comunica con el logger
    int fifo_servidor_cliente; //descriptor de fichero del fifo que envia informacion del servidor al cliente
    int fifo_cliente_servidor; //fd del fifo que comunica el cliente con el servidor para enviar las teclas
    pthread_t thread;
    //pthread_attr_t attr_thread;
    //Metodos
    MundoServidor();
    virtual ~MundoServidor();
    void Init();
    void InitGL();
    void OnKeyboardDown(unsigned char key, int x, int y);
    void OnTimer(int value);
    void OnDraw();
    void RecibeComandosJugador();
};
