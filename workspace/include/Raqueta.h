// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	friend class Disparo;
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	bool recibeDisparo(Plano &p);
};
