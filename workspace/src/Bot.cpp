//Se comunica con el cliente
#include"DatosMemCompartida.h"
#include <sys/types.h>       
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>


int main(){
	DatosMemCompartida *pdatos;
	int fd_mmap;
	if((fd_mmap = open("datosCompartidos", O_RDWR,0777))<0){
		perror("Error al abrir fichero en bot\n");
		return 1;
	}

	pdatos = static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0));
	close(fd_mmap);

	while(1){
		
		if(pdatos->esfera.centro.y < pdatos->raqueta1.getCentro().y){
			pdatos->accion=-1;
		}
		else if(pdatos->esfera.centro.y == pdatos->raqueta1.getCentro().y){
			pdatos->accion=0;
		}
		else if(pdatos->esfera.centro.y > pdatos->raqueta1.getCentro().y){
			pdatos->accion=1;
		}	
		usleep(25000);
	}
	unlink("datosCompartidos");
	
	exit(0);
	
}