#include "Disparo.h"
#include "glut.h"

//Constructor
Disparo::Disparo(direccion d, Vector2D pos):dir(d){
    if (d == derecha)
        velocidad.x = 2;
    else if(d==izquierda)
        velocidad.x = -2;
    x1 = pos.x + 0.2;
    x2 = pos.x - 0.2;
    y1 = pos.y + 0.2;
    y2 = pos.y - 0.2;
}

void Disparo::Mueve(float t){
    //Movimiento en el eje x
    x1 = x1 + velocidad.x*t;
    x2 = x2 + velocidad.x*t;
    //Movimiento en el eje y
    y1 = y1 + velocidad.y*t;
    y2 = y2 + velocidad.y*t;
}

void Disparo::Dibuja(){
    glColor3f(r,g,b);
	glDisable(GL_LIGHTING);
	
	glBegin(GL_POLYGON);
		glVertex3d(x1,y1,1);
		glVertex3d(x1,y2,1);
		glVertex3d(x2,y2,1);	
		glVertex3d(x2,y1,1);	
    glEnd();
}