//Se comunica con el servidor
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>      
#include <iostream>   
#include <unistd.h>
#include <string>
#include "Puntuacion.h"


int main(){
    int fifo;
    Puntuacion puntos;
    //Creamos la tuberia con nombre
    if(mkfifo("fifo_logger", 0600)<0){
        perror("No se puede abrir el fifo");
        return 1;
    }

    //Abrimos la tuberia en modo lectura
    if((fifo=open("fifo_logger", O_RDONLY))<0){
        perror("No se puede abrir el fifo");
        return 1;
    }

    //Leemos los datos
    while(read(fifo, &puntos, sizeof(puntos))!=0){
        if(puntos.ultimpoPunto==1){printf("Jugador 1 ha anotado un punto, lleva %d\n", puntos.jugador1);}
        if(puntos.ultimpoPunto==2){printf("Jugador 2 ha anotado un punto, lleva %d\n", puntos.jugador2);}
    }

    //Destruimos la tuberia
    close(fifo);
    unlink("fifo_logger");
    return 0;
}
