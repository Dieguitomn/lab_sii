#include "glut.h"
#include "MundoCliente.h"
#include "Puntuacion.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>        
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
//Variable global numero disparos
/////////////////////////////////////////////////////////////////////
#define NMAXDISPAROS 10
int nDisparosJ1=0;
int nDisparosJ2=0;
#define NMAXESFERAS 2
#define PENALIZACION -0.1
#define PERIODO 75
#define MAXGOLPES 5
int golpes1=0;
int golpes2=0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MundoCliente::MundoCliente()
{
	Init();
}

MundoCliente::~MundoCliente()
{
    //Cerramos la tubería entre servidor y cliente
    close(fifo_servidor_cliente);
    unlink("fifo_servidor_cliente");
    //Cerramos la tubería entre cliente y servidor
    close(fifo_cliente_servidor);
    unlink("fifo_cliente_servidor");
	//cerramos el enlace proyectado en memoria
    munmap(pdatosCompartidos,sizeof(datosCompartidos));
	unlink("datosCompartidos");
}

void MundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void MundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;

	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();
	for(i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoCliente::OnTimer(int value)
{	
	/////////////////////FIFO/////////////////////
	//Leemos los datos del fifo
    char cad[200]; //cadena para almacenar la información
    read(fifo_servidor_cliente, cad, sizeof(cad));
    //Actualizamos los atributos con los datos recibidos
    sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);

	/////////////////////MMAP///////////////////////
    //Actualizamos los datos del fichero proyectado en memoria
	pdatosCompartidos->esfera = esfera;
	pdatosCompartidos->raqueta1 = jugador1;
	if(pdatosCompartidos->accion == -1){OnKeyboardDown('s',0,0);}
	else if(pdatosCompartidos->accion == 0){}
	else if(pdatosCompartidos->accion == 1){OnKeyboardDown('w',0,0);}
	
}

void MundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'b':esferas.push_back(new Esfera);break;
	case 'd':
		if(nDisparosJ1<NMAXDISPAROS){
		disparos.push_back(new Disparo(derecha,jugador1.getCentro()));
		nDisparosJ1+=1;
		}	
		break;
	case 'k':
		if(nDisparosJ2<NMAXDISPAROS){
		disparos.push_back(new Disparo(izquierda,jugador2.getCentro())); 
		nDisparosJ2+=1;
		}
		break;
	}*/
	char cad[100];
	sprintf(cad,"%c", key);
	write(fifo_cliente_servidor,&key, sizeof(key));
}

void MundoCliente::Init()
{
		////////////////PROYECCIÓN EN MEMORIA/////////////////
	//Creamos un fichero de datos compartidos 
	int fd_mmap;
	if((fd_mmap=open("datosCompartidos", O_CREAT|O_RDWR|O_TRUNC, 0600))<0){
		perror("Error al abrir el fichero de datos compartidos\n");
		return ;
	}
	//Damos valor inicial a los datos compartidos 
	datosCompartidos.esfera=esfera;
	datosCompartidos.raqueta1=jugador1;
	datosCompartidos.accion=0;
	if(write(fd_mmap, &datosCompartidos, sizeof(datosCompartidos))<0){
		perror("Error al escribir en el fichero de datos compartidos\n");
		return ;
	}
	//Hacemos la proyección en memoria
	pdatosCompartidos= static_cast<DatosMemCompartida*> (mmap(NULL, sizeof(datosCompartidos),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap,0));
	close(fd_mmap);
	
	////////////////FIFO///////////////////
    //Creamos la tuberia entre servidor y cliente
    if(mkfifo("fifo_servidor_cliente", 0777)<0){
        perror("No se puede crear el fifo servidor-cliente\n");
        return ;
    }
	//Abrimos la tuberia en modo lectura
	if((fifo_servidor_cliente=open("fifo_servidor_cliente", O_RDONLY)) < 0){
		perror("Error al abrir el fifo en mundo");
		return;
	}

    //Creamos tubería entre cliente y servidor
    if(mkfifo("fifo_cliente_servidor", 0777)<0){
        perror("No se puede crear el fifo cliente-servidor\n");
    }
	//Abrimos la tuberia en modo escritura
	if((fifo_cliente_servidor=open("fifo_cliente_servidor", O_WRONLY)) < 0){
		perror("Error al abrir el fifo en mundo");
		return;
	}    

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

