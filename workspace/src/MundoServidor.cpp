#include "glut.h"
#include "MundoServidor.h"
#include "Puntuacion.h"
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>        
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
//Variable global numero disparos
/////////////////////////////////////////////////////////////////////
#define NMAXDISPAROS 10
int nDisparosJ1=0;
int nDisparosJ2=0;
#define NMAXESFERAS 2
#define PENALIZACION -0.1
#define PERIODO 75
#define MAXGOLPES 5
int golpes1=0;
int golpes2=0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MundoServidor::MundoServidor()
{
	Init();
}

MundoServidor::~MundoServidor()
{
    //Cerramos la tubería entre servidor y logger
    close(logger);
    //unlink("fifo_logger");
	//Cerramos la tubería entre servidor y cliente
    close(fifo_servidor_cliente);
    //unlink("fifo_servidor_cliente");
	//Cerramos la tubería entre cliente y servidor
	//close(fifo_cliente_servidor);
}

void MundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void MundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;

	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();
	for(i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoServidor::OnTimer(int value)
{	
    Puntuacion puntos;
	static int timer = 0;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	for(int i=0;i<paredes.size();i++){
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	for(int i=0;i<esferas.size();i++){
		esferas[i]->Mueve(0.025f);
	}	
	for(int i=0;i<disparos.size();i++){
		disparos[i]->Mueve(0.025f);
	}

	for(int j=0;j<esferas.size();j++){
		for(int i=0;i<paredes.size();i++){
			paredes[i].Rebota(*esferas[j]);
		}
	}
	for(int j=0;j<disparos.size();j++)	{
		bool destruirDisparo=false;
			if(jugador1.recibeDisparo(*disparos[j])&&disparos[j]->dir==izquierda){
				if(golpes1<MAXGOLPES){
				jugador1.y1-=PENALIZACION;
				jugador1.y2+=PENALIZACION;
				golpes1++;
				}	
				destruirDisparo=true;
				nDisparosJ2-=1;
			}
			if(jugador2.recibeDisparo(*disparos[j])&&disparos[j]->dir==derecha){
				if(golpes2<MAXGOLPES){
				jugador2.y1-=PENALIZACION;
				jugador2.y2+=PENALIZACION;
				golpes2++;
				}
				destruirDisparo=true;
				nDisparosJ1-=1;
			}
			if(fondo_izq.Rebota(*disparos[j])||fondo_dcho.Rebota(*disparos[j])){
				if(disparos[j]->dir==derecha){nDisparosJ1-=1;}
				if(disparos[j]->dir==izquierda){nDisparosJ2-=1;}
				destruirDisparo=true;		
			}
			if(destruirDisparo){
				delete disparos[j];
				disparos.erase(disparos.begin()+j);
				disparos.shrink_to_fit();
				timer=0;
			}
			if(timer%PERIODO==0){
				if(golpes1>0){
					jugador1.y1+=PENALIZACION;
					jugador1.y2-=PENALIZACION;
					golpes1--;				
				}
				if(golpes2>0){
					jugador2.y1+=PENALIZACION;
					jugador2.y2-=PENALIZACION;
					golpes2--;				
				}
			
			}
			
		
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	for(int i=0;i<esferas.size();i++){
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);	
	}

	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio = 0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		//El jugador2 marca punto
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.ultimpoPunto=2;
		write(logger, &puntos, sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio = 0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		//El jugador1 marca punto
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.ultimpoPunto=1;
		write(logger, &puntos, sizeof(puntos));
	}

	for(int i=0;i<esferas.size();i++){
		if(fondo_izq.Rebota(*esferas[i])){
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			//El jugador2 marca punto
			puntos.jugador1=puntos1;
			puntos.jugador2=puntos2;
			puntos.ultimpoPunto=2;
			write(logger, &puntos, sizeof(puntos));
		}

		if(fondo_dcho.Rebota(*esferas[i])){	
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos1++;
			//El jugador1 marca punto
			puntos.jugador1=puntos1;
			puntos.jugador2=puntos2;
			puntos.ultimpoPunto=1;
			write(logger, &puntos, sizeof(puntos));
		}					
	}
	timer+=1;

    //Creamos la cadena con los datos que envia el fifo
    char cad[200];
    sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
    //Enviamos los datos
    write(fifo_servidor_cliente, cad, sizeof(cad)); 

	
}

void MundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
   /* switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'b':esferas.push_back(new Esfera);break;
	case 'd':
		if(nDisparosJ1<NMAXDISPAROS){
		disparos.push_back(new Disparo(derecha,jugador1.getCentro()));
		nDisparosJ1+=1;
		}	
		break;
	case 'k':
		if(nDisparosJ2<NMAXDISPAROS){
		disparos.push_back(new Disparo(izquierda,jugador2.getCentro())); 
		nDisparosJ2+=1;
		}
		break;
	}*/
}

void *hilo_comando(void *d){
	MundoServidor* p = (MundoServidor*) d;
	p->RecibeComandosJugador();
}

void MundoServidor::RecibeComandosJugador(){

	//Abrimos la tubería entre cliente y servidor en modo lectura
	if((fifo_cliente_servidor=open("fifo_cliente_servidor", O_RDONLY))<0){
		perror("Error al abrir fifo entre cliente y servidor");
		return ;
	}
	
	while(1){
		usleep(10);
		char cad[100];
		read(fifo_cliente_servidor, cad, sizeof(cad));
		unsigned char key;
		sscanf(cad, "%c", &key);
		if(key=='s') jugador1.velocidad.y = -4;
		if(key=='w') jugador1.velocidad.y = 4;
		if(key=='l') jugador2.velocidad.y = -4;
		if(key=='o') jugador2.velocidad.y = 4;
		if(key=='d'){
			if(nDisparosJ1<NMAXDISPAROS){
				disparos.push_back(new Disparo(derecha,jugador1.getCentro()));
				nDisparosJ1+=1;
			}
		}	
	    
	    if(key=='k'){
			if(nDisparosJ2<NMAXDISPAROS){
				disparos.push_back(new Disparo(izquierda,jugador2.getCentro())); 
				nDisparosJ2+=1;
			}
	    }
	}
}

void MundoServidor::Init()
{
	////////////////FIFO///////////////////
    //Abrimos la tubería entre servidor y logger en modo escritura
    if((logger=open("fifo_logger", O_WRONLY)) < 0){
		perror("Error al abrir el fifo en mundo");
		return;
	}
	//Abrimos la tuberia entre servidor y cliente en modo escfritura
	if((fifo_servidor_cliente=open("fifo_servidor_cliente", O_WRONLY)) < 0){
		perror("Error al abrir el fifo entre servidor y cliente");
		return;
	}
	///////////////////THREADS///////////////////
	//Creamos el thread
	//pthread_attr_init(&attr_thread);
 	//pthread_attr_setdetachstate(&attr_thread, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thread, NULL, hilo_comando, this);


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}




